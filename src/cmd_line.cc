/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  cmd_line.cc
       Description:  Implementation.
           Created:  12/15/2008 07:32:54 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "cmd_line.h"
#include "cmd_add.h"
#include "cmd_about.h"
#include "cmd_help.h"
#include <cstdlib>
#include <cstring>
#include <cerrno>
#include <sys/stat.h>

using namespace std;
using namespace vimicxx;

static cmd_if* parse_cmd_add(int argc, char** argv, string* err);
static cmd_if* parse_cmd_help(int argc, char** argv, string* err);
static cmd_if* parse_cmd_about(int argc, char** argv, string* err);
static cmd_if* parse_cmd_list(int argc, char** argv, string* err);
static cmd_if* parse_cmd_delete(int argc, char** argv, string* err);
static cmd_if* parse_cmd_delete_all(int argc, char** argv, string* err);

cmd_if* vimicxx::parse_cmd_line(int argc, char** argv, string* err)
{
	if (argc < 2) {
		char buf[1024] = {0};
		sprintf(buf, "%s needs at least two arguments, try %s --help",
			argv[0], argv[0]);
		*err = buf;
		return NULL;
	}
	if (strcmp(argv[1], "add") == 0) {
		return parse_cmd_add(argc, argv, err);
	} else if (strcmp(argv[1], "list") == 0) {
		return parse_cmd_list(argc, argv, err);
	} else if (strcmp(argv[1], "delete") == 0) {
		return parse_cmd_delete(argc, argv, err);
	} else if (strcmp(argv[1], "delete-all") == 0) {
		return parse_cmd_delete_all(argc, argv, err);
	} else if (strcmp(argv[1], "about") == 0) {
		return parse_cmd_about(argc, argv, err);
	} else if (strcmp(argv[1], "--help") == 0) {
		return parse_cmd_help(argc, argv, err);
	} else {
		return NULL;
	}
}

cmd_if* parse_cmd_add(int argc, char** argv, string* err)
{
	if (argc < 5) {
		*err = "cmd: add project <project-name> <path to project> \n";
		return NULL;
	} else if (strcmp(argv[2], "project") != 0) {
		*err = "cmd: add project <project-name> <path to project> \n";
		return NULL;
	} else {
		struct stat st;
		if (stat(argv[4], &st) != 0) {
			*err = strerror(errno);
			return NULL;
		} else if (! S_ISDIR(st.st_mode)) {
			*err = "path ";
			*err += argv[4];
			*err += "is not a directory";
			return NULL;
		} else {
			vector<string> e;
			e.push_back(".c");
			e.push_back(".cpp");
			e.push_back(".cc");
			e.push_back(".h");
			return new cmd_add(argv[3], argv[4], e);
		}
	}
	return NULL;
}

cmd_if* parse_cmd_list(int argc, char** argv, string* err)
{
	return NULL;
}

cmd_if* parse_cmd_delete(int argc, char** argv, string* err)
{
	return NULL;
}

cmd_if* parse_cmd_delete_all(int argc, char** argv, string* err)
{
	return NULL;
}

cmd_if* parse_cmd_about(int argc, char** argv, string* err)
{
	return new cmd_about(argv[0]);
}

cmd_if* parse_cmd_help(int argc, char** argv, string* err)
{
	return new cmd_help(argv[0]);
}
