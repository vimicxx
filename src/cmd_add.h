/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  cmd_add.h
       Description:  Add project command.
           Created:  12/15/2008 08:07:47 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  CMD_ADD_INC
#define  CMD_ADD_INC
#include <vector>
#include <string>
#include "cmd_if.h"
namespace vimicxx
{
class cmd_add: public cmd_if
{
	public:
	cmd_add(const std::string& prj_name_, const std::string& prj_path_,
		const std::vector<std::string>& ext_list_);
	bool execute(conf* cnf) const;
	const std::string err_msg() const;
	~cmd_add();
	private:
	const cmd_add& operator=(const cmd_add&);
	private:
	std::string project_name;
	std::string project_path;
	mutable std::string error;
	const std::vector<std::string> ext_list;

};
}
#endif   // ----- #ifndef CMD_ADD_INC  -----
