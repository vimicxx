/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  cmd_if.h
       Description:  A command interface.
           Created:  12/15/2008 07:34:00 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  CMD_IF_INC
#define  CMD_IF_INC
#include <string>
namespace vimicxx
{

class conf;
class cmd_if
{
	public:
	virtual bool execute(conf* conf) const = 0;
	virtual const std::string err_msg() const = 0;
	virtual ~cmd_if()
	{
	}
};

}
#endif   // ----- #ifndef CMD_IF_INC  -----
