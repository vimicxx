/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  cmd_line.h
       Description:  Parse the command line.
           Created:  12/15/2008 07:28:16 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "cmd_if.h"
#include <string>
namespace vimicxx
{
cmd_if* parse_cmd_line(int argc, char** argv, std::string* err);
};
