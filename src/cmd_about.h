/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  cmd_about.h
       Description:  An about command.
           Created:  12/15/2008 11:13:36 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  CMD_ABOUT_INC
#define  CMD_ABOUT_INC
#include <vector>
#include <string>
#include "cmd_if.h"
namespace vimicxx
{
class cmd_about: public cmd_if
{
	public:
	cmd_about(const std::string& n);
	bool execute(conf* cnf) const;
	const std::string err_msg() const;
	~cmd_about();
	private:
	const cmd_about& operator=(const cmd_about&);
	private:
	std::string name;
	const std::string error;
};
}
#endif   // ----- #ifndef CMD_ABOUT_INC  -----
