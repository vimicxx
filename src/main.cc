/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  main.cc
       Description:  Vimicxx main.
           Created:  09/01/2008 06:35:01 PM PDT
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include <unistd.h>
#include <pwd.h>
#include <sys/stat.h>
#include <string>
#include <cstdlib>
#include "conf.h"
#include "ctags_db.h"
#include "cmd_line.h"
#include "cmd_if.h"
using namespace std;
using namespace vimicxx;

bool vimicxx_init(string*);
void config(configfile*);

int main(int argc, char* argv[])
{
	string DOTVIMICXX_PATH;
	if (!vimicxx_init(&DOTVIMICXX_PATH)) {
		return 1;
	}
	conf config(DOTVIMICXX_PATH);
	string err;
	cmd_if* cmd = parse_cmd_line(argc, argv, &err);

	if (cmd != 0) {
		cmd->execute(&config);
		delete cmd;
	}

#if  0     // ----- #if 0 : If0Label_1 ----- 
	vector<string> tmp;
	vector<string> extensions;
	extensions.push_back(".c");
	extensions.push_back(".cpp");
	extensions.push_back(".h");
	config.add_project("test", "/home/ahmed/data", tmp);
	config.add_project("test3", "/home/ahmed/data/oss/vimicxx", extensions);

	string name, prj_path, prj_cache, prj_flist, prj_tags_db;
	config.get_project_path("test4", &prj_path);
	config.get_project_cache_dir("test4", &prj_cache);
	config.get_project_flist("test4", &prj_flist);
	config.get_project_tags_db("test4", &prj_tags_db);
	ctags_db ctags("test4", prj_path, prj_cache, prj_flist,
			 prj_tags_db, extensions);
	ctags.create();

	time_t start = time(0);
	ctags.update();
	time_t end = time(0);
	printf("time : %d\n", end - start);
#endif     // ----- #if 0 : If0Label_1 ----- 

	return 0;
}

/*
 * Check for the .vimicxx directory, create it if it
 * doesn't exist, checks for DOTVIMICXX_PATH
 */
bool vimicxx_init(string* vimicxx_path)
{
	char* tmp;
	if ((tmp = getenv("DOTVIMICXX_PATH")) != NULL) {
		*vimicxx_path = tmp;
	} else {
		passwd* tmp2 = getpwuid(geteuid());
		*vimicxx_path = tmp2->pw_dir;
		*vimicxx_path += "/.vimicxx";
	}

	struct stat tmp_stat;
	if ((stat(vimicxx_path->c_str(), &tmp_stat) == 0) &&
	   (S_ISDIR(tmp_stat.st_mode))) {
		return true;
	} else if (mkdir(vimicxx_path->c_str(), S_IRUSR | S_IWUSR | S_IXUSR |
			 S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH) == 0) {
		return true;
	} else {
		perror("mkdir error");
		return false;
	}
}
