/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  cmd_help.h
       Description:  A help command.
           Created:  12/15/2008 11:02:42 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  CMD_HELP_INC
#define  CMD_HELP_INC
#include <vector>
#include <string>
#include "cmd_if.h"
namespace vimicxx
{
class cmd_help: public cmd_if
{
	public:
	cmd_help(const std::string& n);
	bool execute(conf* cnf) const;
	const std::string err_msg() const;
	~cmd_help();
	private:
	const std::string name;
	const std::string error;
	private:
	const cmd_help& operator=(const cmd_help&);
};
}
#endif   // ----- #ifndef CMD_HELP_INC  -----
