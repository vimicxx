/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  generic_exception.h
       Description:  A generic exception class.
           Created:  08/19/2008 11:43:10 PM PDT
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  GENERIC_EXCEPTION_INC
#define  GENERIC_EXCEPTION_INC
#include <exception>
#include <string>
class generic_exception: public std::exception
{
	public:
	generic_exception(const std::string& m) throw():
		msg(m)
	{
	}
	const char* what() const throw()
	{
		return msg.c_str();
	}
	~generic_exception() throw ()
	{
	}
	private:
	std::string msg;
};
#endif   // ----- #ifndef GENERIC_EXCEPTION_INC  -----
