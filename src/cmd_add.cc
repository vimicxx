/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  cmd_add.cc
       Description:  Implementation.
           Created:  12/15/2008 08:24:59 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "cmd_add.h"
#include "conf.h"
#include "ctags_db.h"
using namespace std;
using namespace vimicxx;

cmd_add::cmd_add (const string& prj_name, const string& prj_path,
		  const vector<string>& ext_list_):
	project_name(prj_name), project_path(prj_path), ext_list(ext_list_)
{
}

bool
cmd_add::execute (conf* cnf) const
{
	vector<string> projects = cnf->get_project_list();
	vector<string>::const_iterator pos;
	vector<string>::const_iterator end = projects.end();
	for (pos = projects.begin(); pos != end; ++pos) {
		if (*pos == project_name) {
			error = "duplicate project name: " + project_name;
			return false;
		}
	}
	int e = cnf->add_project(project_name, project_path, ext_list);
	if (e != 0) {
		error = cnf->err_to_string(e);
		return false;
	}
	string path, cache, index, flist, tags_db;
	cnf->get_project_path(project_name, &path);
	cnf->get_project_cache_dir(project_name, &cache);
	cnf->get_project_index(project_name, &index);
	cnf->get_project_flist(project_name, &flist);
	cnf->get_project_tags_db(project_name, &tags_db);
	ctags_db db(project_name, path, cache, flist, tags_db, ext_list);
	return db.create();
}

const string
cmd_add::err_msg () const
{
	return error;
}

cmd_add::~cmd_add ()
{
}

