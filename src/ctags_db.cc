/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  ctags_db.cc
       Description:  Implementation.
           Created:  09/17/2008 11:01:30 PM PDT
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "ctags_db.h"
#include "tree_builder.h"
#include "fs_item.h"
#include <cstring>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <cstdlib>
using namespace std;

ctags_db::ctags_db (const string& prj_, const string& prj_path_, const
		      string& prj_cache_, const string& prj_flist_, const
		      string& prj_ctags_db_, const vector<string>&
		      extensions_):
	prj(prj_), prj_path(prj_path_), prj_cache(prj_cache_),
	prj_flist(prj_flist_), prj_ctags_db(prj_ctags_db_),
	extension_list(extensions_)
{
}

bool
ctags_db::create ()
{
	tree_builder* builder = tree_builder::get_instance();
	const fs_item* tree = builder->build(prj_path, extension_list,
					     &file_list);
	FILE* tmp;
	if ((tmp = fopen(prj_flist.c_str(), "w")) == 0) {
		perror("fopen");
		return false;
	}
	for (int i = 0; i < file_list.size(); ++i) {
		fprintf(tmp, "%s\n", file_list[i].c_str());
	}
	fclose(tmp);
	return build_db(prj_flist, prj_ctags_db);
}

bool
ctags_db::add_file (const string& new_file)
{
	FILE* tmp;
	if ((tmp = fopen(prj_flist.c_str(), "a+")) == 0) {
		perror("fopen");
		return false;
	}
	fprintf(tmp, "%s\n", new_file.c_str());
	fclose(tmp);
	return build_db(prj_flist, prj_ctags_db);
}

bool
ctags_db::remove_file(const string& old_file)
{
	FILE *tmp1, *tmp2;
	if ((tmp1 = fopen(prj_flist.c_str(), "r")) == 0) {
		perror("fopen");
		return false;
	}
	string tmpfile = prj_cache;
	tmpfile += "/tmp.txt";
	if ((tmp1 = fopen(tmpfile.c_str(), "w")) == 0) {
		perror("fopen");
		return false;
	}

	const unsigned MAX = 2048;
	char* buf = (char*) malloc(sizeof(char) * MAX);
	if (! buf) {
		perror("malloc");
		return false;
	}
	memset(buf, 0, sizeof(char) * MAX);
	size_t n = MAX;
	while (getline(&buf, &n, tmp1)) {
		if (strcmp(buf, old_file.c_str()) == 0) {
			continue;
		}
		fprintf(tmp2, "%s", buf);
	}

	fclose(tmp1);
	fclose(tmp2);
	free(buf);
	if (rename(tmpfile.c_str(), prj_flist.c_str()) != 0) {
		perror("rename");
		return false;
	}
	return build_db(prj_flist, prj_ctags_db);
}

bool
ctags_db::update ()
{
	return build_db(prj_flist, prj_ctags_db);
}

bool
ctags_db::build_db (const string& flist_file, const string& db_file) const
{
	vector<string> args;
	string tmp;
	args.push_back("--c++-kinds=+p");
	args.push_back("--fields=+iaS");
        args.push_back("--extra=+q");
	args.push_back("-f");
	//tmp += db_file;
	args.push_back(db_file);
	//tmp = "-L";
	args.push_back("-L");
	//tmp += flist_file;
	args.push_back(flist_file);
	return invoke_ctags(args);
}

bool
ctags_db::invoke_ctags (const vector<string>& args) const
{
	char* const * argv = build_argv(args);
	pid_t pid = 0;
	if ((pid = fork()) == -1) {
		perror("fork");
	} else if (pid != 0) {
		int status;
		waitpid(pid, &status, 0);
		if (WEXITSTATUS(status) != 0) {
			return false;
		} else {
			return true;
		}
	} else if (execvp("ctags", argv) == -1 ) {
		perror("execvp");
		_exit(1);
	}
	return false;
}

char**
ctags_db::build_argv (const vector<string>& args) const
{
	char** argv;
	argv = new char*[args.size() + 2];
	bzero(argv, (args.size() + 2) * sizeof(char*));
	argv[0] = strdup("ctags");
	for (int i = 0; i < args.size(); ++i) {
		argv[i + 1] = strdup(args[i].c_str());
	}
	return argv;
}

ctags_db::~ctags_db ()
{
}

