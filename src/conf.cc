/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  conf.cc
       Description:  Implementation.
           Created:  09/07/2008 05:31:00 PM PDT
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "conf.h"
#include "configfile.h"
#include <errno.h>
#include <sys/stat.h>
#include <algorithm>
using namespace std;
using namespace vimicxx;

conf::conf(const string& vimicxx_dir):
	config(0), vimicxx_home(vimicxx_dir),
	conf_path(vimicxx_home + "/vimicxx.conf")
{
	config = new configfile(conf_path);
	struct stat tmp_stat;
	if (stat(conf_path.c_str(), &tmp_stat) != 0) {
		string sec_name = "vimicxx.conf";
		config->add_section(sec_name);
		config->add_key(sec_name, "version", "1.0");
		config->add_key(sec_name, "conf_file", conf_path);
	}
}

const std::string
conf::err_to_string(int e)
{
	switch (e) {
	case Ok:
		return "All ok";
		break;
	case Err_DuplicatePrjName:
		return "A project with this name already exists";
		break;
	case Err_MissingPrjPath:
		return "Directory does not exist or is unreadable";
		break;
	}
}

int
conf::add_project(const string& prj_name, const string& prj_path,
			  const vector<string>& ext_list)
{
	string new_project = "project." + prj_name;
	vector<string> projects = config->get_sections();
	if (find(projects.begin(), projects.end(), new_project) !=
							projects.end()) {
		return Err_DuplicatePrjName;
	}
	struct stat tmp_stat;
	if (stat(prj_path.c_str(), &tmp_stat) != 0) {
		return Err_MissingPrjPath;
	}


	config->add_section(new_project);
	string prj_cache = vimicxx_home + "/" + prj_name;
	int tmp = stat(prj_cache.c_str(), &tmp_stat);
	if ((tmp == 0) || (errno != ENOENT)) {
		return Err_DuplicatePrjCache;
	} else if (mkdir(prj_cache.c_str(), S_IRUSR | S_IWUSR | S_IXUSR |
			 S_IRGRP | S_IXGRP | S_IROTH | S_IXOTH) != 0) {
		perror("mkdir");
		return Err_Mkdir;
	}

	config->add_key(new_project, "path", prj_path);
	config->add_key(new_project, "cache_dir", prj_cache);
	config->add_key(new_project, "index", prj_cache + "/index");
	config->add_key(new_project, "file_list", prj_cache + "/file_list.txt");
	config->add_key(new_project, "tags_db", prj_cache + "/tags_db.txt");
	return Ok;
}

bool
conf::get_project_path(const string& prj, string* path) const
{
	string secname = "project." + prj;
	return config->get_key_value(secname, "path", path);
}
bool
conf::get_project_cache_dir(const string& prj, string* cache_dir)
	const
{
	string secname = "project." + prj;
	return config->get_key_value(secname, "cache_dir", cache_dir);
}

bool
conf::get_project_index(const string& prj, string* index) const
{
	string secname = "project." + prj;
	return config->get_key_value(secname, "index", index);
}

bool
conf::get_project_flist(const string& prj, string* flist) const
{
	string secname = "project." + prj;
	return config->get_key_value(secname, "file_list", flist);
}
bool
conf::get_project_tags_db(const string& prj, string* tags_db)
	const
{
	string secname = "project." + prj;
	return config->get_key_value(secname, "tags_db", tags_db);
}

const vector<string>
conf::get_project_list () const
{
	return config->get_sections();
}

conf::~conf ()
{
	config->save();
}

