/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  conf.h
       Description:  Implements vimicxx configuration file policy.
           Created:  09/07/2008 05:03:11 PM PDT
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  VIMICXX_CONF_INC
#define  VIMICXX_CONF_INC
#include <string>
#include <vector>
class configfile;
namespace vimicxx
{

class conf
{
	public:
	conf(const std::string& vimicxx_dir);
	static const std::string err_to_string(int e);
	int add_project(const std::string& prj_name, const std::string&
			 prj_path, const std::vector<std::string>& ext_list);
	bool get_project_path(const std::string& prj, std::string* path) const;
	bool get_project_cache_dir(const std::string& prj, std::string* cache)
									const;
	bool get_project_index(const std::string& prj, std::string* index)
									const;
	bool get_project_flist(const std::string& prj, std::string* flist)
									const;
	bool get_project_tags_db(const std::string& prj, std::string*
				   tags_db) const;
	const std::vector<std::string> get_project_list() const;
	~conf();
	private:
	enum {
		Ok,
		Err_DuplicatePrjName,
		Err_MissingPrjPath,
		Err_DuplicatePrjCache,
		Err_Mkdir
	};
	private:
	configfile* config;
	const std::string vimicxx_home;
	const std::string conf_path;

	private:
	conf(const conf&);
	const conf& operator=(const conf&);
};

}
#endif   // ----- #ifndef VIMICXX_CONF_INC  -----
