/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  ctags_db.h
       Description:  A ctags db abstraction layer.
           Created:  09/07/2008 04:44:50 PM PDT
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  CTAGS_DB_INC
#define  CTAGS_DB_INC
#include <string>
#include <vector>
class ctags_db
{
	public:
	ctags_db(const std::string& prj_, const std::string& prj_path_, const
		  std::string& prj_cache_, const std::string& prj_flist_,
		  const std::string& prj_ctags_db_, const
		  std::vector<std::string>& extensions_);
	bool create();
	bool add_file(const std::string& new_file);
	bool remove_file(const std::string& new_file);
	bool update();
	~ctags_db();
	private:
	const std::string prj, prj_path, prj_cache, prj_flist, prj_ctags_db;
	const std::vector<std::string> extension_list;
	std::vector<std::string> file_list;
	private:
	ctags_db(const ctags_db&);
	ctags_db& operator=(const ctags_db&);
	bool build_db(const std::string& flist_file, const std::string&
		      db_file) const;
	bool update_db(const std::string& flist_file, const std::string&
		       db_file) const;
	bool invoke_ctags(const std::vector<std::string>& args) const;
	char** build_argv (const std::vector<std::string>& args) const;
};
#endif   // ----- #ifndef CTAGS_DB_INC  -----
