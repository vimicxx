/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  cmd_help.cc
       Description:  Implementation.
           Created:  12/15/2008 11:04:49 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "cmd_help.h"
#include "conf.h"
#include <cstdio>
using namespace std;
using namespace vimicxx;

cmd_help::cmd_help (const string& n):
	name(n), error("")
{
}

bool
cmd_help::execute (conf* cnf) const
{
	printf("usage: %s <command> <command-options>\n", name.c_str());
	printf("\n");
	printf("\tadd project <project-name> <project-path>\n");
	printf("\tlist [<project-name>]\n");
	printf("\tdelete [<project-name>]\n");
	printf("\tdelete-all\n");
	printf("\tabout\n");
	printf("\t--help\n\n");
	return true;
}

const string
cmd_help::err_msg () const
{
	return error;
}

cmd_help::~cmd_help ()
{
}

