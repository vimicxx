/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  cmd_about.cc
       Description:  Implementation
           Created:  12/15/2008 11:22:18 PM PST
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "cmd_about.h"
#include "conf.h"
#include <cstdio>
#include "config.h"
using namespace std;
using namespace vimicxx;

cmd_about::cmd_about (const string& n):
	name(n), error("")
{
}

bool
cmd_about::execute (conf* cnf) const
{
	printf(PACKAGE_NAME);
	printf(" by Ahmed Badran (" PACKAGE_BUGREPORT
	       ") version "PACKAGE_VERSION"\n");
	printf("licensed under the FreeBSD License (see LICENSE)\n");
	printf("\tHopefully you will find it useful :)\n");
	return true;
}

const string
cmd_about::err_msg () const
{
	return error;
}

cmd_about::~cmd_about ()
{
}

