/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  fs_item_file.cc
       Description:  Implementation.
           Created:  08/18/2008 10:19:15 PM PDT
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "fs_item_file.h"
#include "generic_exception.h"
using namespace std;

fs_item_file::fs_item_file (const string& name_):
	fs_item(name_), name(name_)
{
}

const string
fs_item_file::get_name () const
{
	return name;
}

fs_item::FS_ItemType
fs_item_file::get_type () const
{
	return fs_item::FS_ItemType_File;
}

const fs_item*
fs_item_file::get_parent () const
{
	return 0;
}

const vector<const fs_item*>
fs_item_file::get_children () const
{
	/* should throw exception here */
	string tmp = "error: attempting to get the children of file item: ";
	tmp += get_name();
	throw generic_exception(tmp);
}

void
fs_item_file::add_child (fs_item*)
{
	/* should throw exception here */
	string tmp = "error: attempting to add a child to file item: ";
	tmp += get_name();
	throw generic_exception(tmp);
}

fs_item_file::~fs_item_file ()
{
}

