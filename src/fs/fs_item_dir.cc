/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  fs_item_dir.cc
       Description:  Implementation.
           Created:  08/22/2008 09:20:08 PM PDT
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "fs_item_dir.h"
using namespace std;

fs_item_dir::fs_item_dir(const string& name_):
	fs_item(name_), name(name_)
{
}

const string
fs_item_dir::get_name() const
{
	return name;
}

fs_item::FS_ItemType
fs_item_dir::get_type () const
{
	return FS_ItemType_Dir;
}

const fs_item*
fs_item_dir::get_parent() const
{
	return 0;
}
const vector<const fs_item*>
fs_item_dir::get_children() const
{
	return vector<const fs_item*>(children.begin(), children.end());
}

void
fs_item_dir::add_child (fs_item* child)
{
	children.push_back(child);
}

fs_item_dir::~fs_item_dir ()
{
}

