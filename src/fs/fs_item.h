/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  fs_item.h
       Description:  A file system item interface.
           Created:  08/17/2008 12:43:52 PM PDT
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  FS_ITEM_INC
#define  FS_ITEM_INC
#include <string>
#include <vector>

class fs_item
{
	public:
	enum FS_ItemType {
		FS_ItemType_File,
		FS_ItemType_Dir
	};
	fs_item(const std::string& name_)
	{
	}
	virtual const std::string get_name() const = 0;
	virtual FS_ItemType get_type() const = 0;
	virtual const fs_item* get_parent() const = 0;
	virtual const std::vector<const fs_item*> get_children() const = 0;
	virtual void add_child(fs_item*) = 0;
	virtual ~fs_item()
	{
	}
};

extern bool is_dir(const fs_item&);
extern bool is_file(const fs_item&);

#endif   // ----- #ifndef FS_ITEM_INC  -----
