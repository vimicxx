/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  tree_builder.h
       Description:  A tree builder, returns a tree filtered by patterns.
           Created:  08/23/2008 07:22:19 PM PDT
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  TREE_BUILDER_INC
#define  TREE_BUILDER_INC
#include <string>
#include <vector>
class fs_item;
class tree_builder
{
	public:
	static tree_builder* get_instance();
	fs_item* build(const std::string& path, const
		       std::vector<std::string>& patterns,
		       std::vector<std::string>* file_list) const;
	~tree_builder();
	private:
	tree_builder();
	static tree_builder* instance;
	void build_dir_tree(const std::string& path, fs_item* dir_item, const
			    std::vector<std::string>& patterns,
			    std::vector<std::string>* file_list) const;
	bool matches(const char*, const std::vector<std::string>& patterns)
		const;
};
#endif   // ----- #ifndef TREE_BUILDER_INC  -----
