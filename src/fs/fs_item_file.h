/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  fs_item_file.h
       Description:  A file class, implements the fs_item interface.
           Created:  08/18/2008 09:48:40 PM PDT
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  FS_ITEM_FILE_INC
#define  FS_ITEM_FILE_INC
#include "fs_item.h"
class fs_item_file: public fs_item
{
	public:
	fs_item_file(const std::string& name_);
	const std::string get_name() const;
	fs_item::FS_ItemType get_type() const;
	const fs_item* get_parent() const;
	const std::vector<const fs_item*> get_children() const;
	void add_child(fs_item*);
	~fs_item_file();
	private:
	const std::string name;
};
#endif   // ----- #ifndef FS_ITEM_FILE_INC  -----
