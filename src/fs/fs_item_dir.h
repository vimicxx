/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  fs_item_dir.h
       Description:  A file system directory item.
           Created:  08/22/2008 09:08:23 PM PDT
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#ifndef  FS_ITEM_DIR_INC
#define  FS_ITEM_DIR_INC
#include <string>
#include <vector>
#include "fs_item.h"
class fs_item_dir: public fs_item
{
	public:
	fs_item_dir(const std::string& name_);
	const std::string get_name() const;
	fs_item::FS_ItemType get_type() const;
	const fs_item* get_parent() const;
	const std::vector<const fs_item*> get_children() const;
	void add_child(fs_item*);
	~fs_item_dir();
	private:
	const std::string name;
	std::vector<fs_item*> children;

};
#endif   // ----- #ifndef FS_ITEM_DIR_INC  -----
