/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  fs_item.cc
       Description:  Implementation.
           Created:  08/17/2008 01:59:30 PM PDT
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "fs_item.h"

bool is_dir(const fs_item& item)
{
	return item.get_type() == fs_item::FS_ItemType_Dir ? true : false;
}

bool is_file(const fs_item& item)
{
	return item.get_type() == fs_item::FS_ItemType_File ? true : false;
}

