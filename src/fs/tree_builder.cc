/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  tree_builder.cc
       Description:  Implementation
           Created:  08/23/2008 09:24:28 PM PDT
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include <dirent.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>
#include <cstdlib>
#include <cstring>
#include "tree_builder.h"
#include "fs_item_dir.h"
#include "fs_item_file.h"
using namespace std;

tree_builder* tree_builder::instance = 0;

tree_builder::tree_builder ()
{
}

tree_builder*
tree_builder::get_instance ()
{
	if (!instance)
		instance = new tree_builder;
	return instance;
}

fs_item*
tree_builder::build (const string& path, const vector<string>& patterns,
		     vector<string>* file_list) const
{
	fs_item_dir* dir = new fs_item_dir(path);
	build_dir_tree(path, dir, patterns, file_list);
	return dir;
}

void
tree_builder::build_dir_tree (const string& path, fs_item* dir, const
			      vector<string>& patterns, vector<string>*
			      file_list) const
{
	DIR* dir_handle = 0;
	if ((dir_handle = opendir(path.c_str())) == 0) {
		perror("opendir");
		return;
	}
	long path_max = pathconf(path.c_str(), _PC_NAME_MAX);
	char* buf = new char[sizeof(struct dirent) + path_max + 1];
	memset(buf, 0, sizeof(struct dirent) + path_max + 1);
	struct dirent* entry = 0;
	string tmp;
	struct stat stat_buf;
	while ((readdir_r(dir_handle, reinterpret_cast<struct dirent*>(buf),
			&entry) == 0) && entry) {
		tmp = path + "/" + entry->d_name;
		memset(&stat_buf, 0, sizeof(stat_buf));
		if (stat(tmp.c_str(), &stat_buf) == -1) {
			perror("stat");
			break;
		}
		if (S_ISDIR(stat_buf.st_mode) &&
		    (strcmp(entry->d_name, ".") != 0) &&
		    (strcmp(entry->d_name, "..") != 0)) {
			fs_item_dir* item = new fs_item_dir(entry->d_name);
			build_dir_tree(tmp, item, patterns, file_list);
			dir->add_child(item);
		} else if (matches(entry->d_name, patterns)){
			file_list->push_back(tmp);
			fs_item_file* item = new fs_item_file(entry->d_name);
			dir->add_child(item);
		}
	}
	delete [] buf;
	closedir(dir_handle);
}

bool
tree_builder::matches (const char* entry, const vector<string>& patterns)
	const
{
	const unsigned entry_len = strlen(entry);
	unsigned pattern_len = 0;
	vector<string>::const_iterator pos;
	vector<string>::const_iterator end = patterns.end();
	for (pos = patterns.begin(); pos != end; ++pos) {
		pattern_len = (*pos).length();
		if (pattern_len > entry_len) {
			continue;
		}
		if (strcmp(&entry[entry_len - pattern_len], (*pos).c_str()) ==
		    0) {
			return true;
		}
	}
	return false;
}

tree_builder::~tree_builder ()
{
}


