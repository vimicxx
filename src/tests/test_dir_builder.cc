/*******************************************************************************
********************************************************************************

        Copyright (c) 2008 Ahmed S. Badran

	Licensed under the FreeBSD License (see LICENSE)

          Filename:  test_dir_builder.cc
       Description:  Test the directory builder.
           Created:  08/23/2008 10:39:50 PM PDT
            Author:  Ahmed S. Badran (Ahmed B.), ahmed.badran@gmail.com

********************************************************************************
*******************************************************************************/
#include "tree_builder.h"
#include <vector>
#include <string>
#include "fs_item.h"
#include <cstdio>
using namespace std;
int main(int argc, char** argv)
{
	vector<string> tmp, file_list;
	tree_builder* builder = tree_builder::get_instance();;
	tmp.push_back(".h");
	tmp.push_back(".c");
	tmp.push_back(".cpp");
	fs_item* tree = builder->build(argv[1], tmp, &file_list);
	for (int i = 0; i < file_list.size(); ++i) {
		printf("%s\n", file_list[i].c_str());
	}
	return 0;
}
